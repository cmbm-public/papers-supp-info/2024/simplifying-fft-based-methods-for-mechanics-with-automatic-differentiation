import sys
import os
from memory_profiler import profile
from memory_profiler import memory_usage

base_dir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(os.path.join(base_dir, "fft_helpers/"))

# jax related imports
import jax

jax.config.update("jax_compilation_cache_dir", "/cluster/scratch/mpundir/jax-cache")
import jax.numpy as jnp

jax.config.update("jax_enable_x64", True)  # use double-precision
jax.config.update("jax_platforms", "cpu")
import numpy as np

import functools
from jax.typing import ArrayLike
from jax import Array

from skimage.morphology import rectangle

# for profiling
import timeit
import tracemalloc

# from fft helpers
from projection_operators import compute_Ghat_4_2
import tensor_operators as tensor


import argparse
import perfplot

import random

random.seed(1)


# material parameters + function to convert to grid of scalars
@functools.partial(jax.jit, static_argnames=["soft", "hard"])
def param(X, soft, hard):
    return soft * jnp.ones_like(X) * (X) + hard * jnp.ones_like(X) * (1 - X)


def place_circle(matrix, n, r, x_center, y_center):
    for i in range(n):
        for j in range(n):
            if (i - x_center) ** 2 + (j - y_center) ** 2 <= r**2:
                matrix[i][j] = 1


def setup(n):
    x = 10
    r = int(n / 10)  # 20

    if r >= n:
        raise ValueError("Radius r must be less than the size of the matrix n")

    matrix = np.zeros((n, n), dtype=int)
    placed_circles = 0

    while placed_circles < x:
        x_center = random.randint(0, n - 1)
        y_center = random.randint(0, n - 1)

        # Check if the circle fits within the matrix bounds
        if (
            x_center + r < n
            and y_center + r < n
            and x_center - r >= 0
            and y_center - r >= 0
        ):
            previous_matrix = matrix.copy()
            place_circle(matrix, n, r, x_center, y_center)
            if not np.array_equal(previous_matrix, matrix):
                placed_circles += 1

    return matrix


def use_standard_fft_method(structure):
    N = structure.shape[0]
    ndim = len(structure.shape)

    epsbar = 0.12

    # identity tensor (single tensor)
    i = jnp.eye(ndim)

    # identity tensors (grid)
    I = jnp.einsum(
        "ij,xy",
        i,
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )  # 2nd order Identity tensor
    I4 = jnp.einsum(
        "ijkl,xy->ijklxy",
        jnp.einsum("il,jk", i, i),
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )  # 4th order Identity tensor
    I4rt = jnp.einsum(
        "ijkl,xy->ijklxy",
        jnp.einsum("ik,jl", i, i),
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )
    I4s = (I4 + I4rt) / 2.0

    II = tensor.dyad22(I, I)
    I4d = I4s - II / 3.0

    # (inverse) Fourier transform (for each tensor component in each direction)
    @jax.jit
    def fft(x):
        return jnp.fft.fftshift(jnp.fft.fftn(jnp.fft.ifftshift(x), [N, N]))

    @jax.jit
    def ifft(x):
        return jnp.fft.fftshift(jnp.fft.ifftn(jnp.fft.ifftshift(x), [N, N]))

    Ghat4_2 = compute_Ghat_4_2(NN=(N,) * ndim, operator="fourier", length=1.0)

    # material parameters
    K = param(structure, soft=0.833, hard=2 * 0.833)  # bulk      modulus
    μ = param(structure, soft=0.386, hard=2 * 0.386)  # shear     modulus
    H = param(
        structure, soft=2000.0e6 / 200.0e9, hard=2.0 * 2000.0e6 / 200.0e9
    )  # hardening modulus
    sigma_y = param(
        structure, soft=600.0e6 / 200.0e9, hard=2.0 * 600.0e6 / 200.0e9
    )  # initial yield stress
    n = 1.0  # param(
    # structure, soft=1., hard=1.
    # )

    @jax.jit
    def yield_function(ep):
        return sigma_y + H * ep**n

    @jax.jit
    def constitutive(eps, eps_t, epse_t, ep_t):

        # elastic stiffness tensor
        C4e = K * II + 2.0 * μ * I4d

        # trial state
        epse_s = epse_t + (eps - eps_t)
        sig_s = tensor.ddot42(C4e, epse_s)
        sigm_s = tensor.ddot22(sig_s, I) / 3.0
        sigd_s = sig_s - sigm_s * I
        sigeq_s = jnp.sqrt(3.0 / 2.0 * tensor.ddot22(sigd_s, sigd_s))

        # avoid zero division below ("phi_s" is corrected below)
        Z = jnp.where(sigeq_s == 0, True, False)
        sigeq_s = jnp.where(Z == True, 1, sigeq_s)

        # evaluate yield surface, set to zero if elastic (or stress-free)
        sigy = yield_function(ep_t)
        phi_s = sigeq_s - sigy
        phi_s = 1.0 / 2.0 * (phi_s + jnp.abs(phi_s))
        phi_s = jnp.where(Z == True, 0.0, phi_s)
        elastic_pt = jnp.where(phi_s <= 0, True, False)

        # plastic multiplier, based on non-linear hardening
        # - initialize
        dep = phi_s / (3 * μ + H)

        # return map algorithm
        N = 3.0 / 2.0 * sigd_s / sigeq_s
        ep = ep_t + dep
        sig = sig_s - dep * N * 2.0 * μ
        epse = epse_s - dep * N

        # plastic tangent stiffness
        C4ep = (
            C4e
            - 6.0 * (μ**2.0) * dep / sigeq_s * I4d
            + 4.0
            * (μ**2.0)
            * (dep / sigeq_s - 1.0 / (3.0 * μ + H))
            * tensor.dyad22(N, N)
        )

        # consistent tangent operator: elastic/plastic switch
        elastic_pt = elastic_pt.astype(jnp.float64)
        K4 = C4e * elastic_pt + C4ep * (1.0 - elastic_pt)

        # return 3-D stress, 2-D stress/tangent, and history
        return sig, K4, epse, ep

    # functions for the projection 'G', and the product 'G : K : eps'
    @jax.jit
    def G(A2):
        return jnp.real(ifft(tensor.ddot42(Ghat4_2, fft(A2)))).reshape(-1)

    @jax.jit
    def K_deps(depsm, K4):
        return tensor.ddot42(K4, depsm.reshape(ndim, ndim, N, N))

    @jax.jit
    def G_K_deps(depsm, K4):
        return G(K_deps(depsm, K4))

    @functools.partial(jax.jit, static_argnums=(0,))
    def conjugate_gradient(A, b, additional, atol=1e-5):

        b, additional = jax.device_put((b, additional))

        iiter = 0

        def body_fun(state):
            b, p, r, rsold, x, iiter = state
            Ap = A(p, additional)
            alpha = rsold / jnp.vdot(p, Ap)
            x = x + jnp.dot(alpha, p)
            r = r - jnp.dot(alpha, Ap)
            rsnew = jnp.vdot(r, r)
            p = r + (rsnew / rsold) * p
            rsold = rsnew
            iiter = iiter + 1
            return (b, p, r, rsold, x, iiter)

        def cond_fun(state):
            b, p, r, rsold, x, iiter = state
            return jnp.logical_and(jnp.sqrt(rsold) > atol, iiter < 100)

        x = jnp.zeros_like(b)
        r = b - A(x, additional)
        p = r
        rsold = jnp.vdot(r, r)

        b, p, r, rsold, x, iiter = jax.lax.while_loop(
            cond_fun, body_fun, (b, p, r, rsold, x, iiter)
        )
        return x, iiter

    @jax.jit
    def solve_plasticity(state, n):
        deps, b, eps, eps_t, epse_t, ep_t, Fn, K4, sig = state

        error = jnp.linalg.norm(deps) / Fn
        jax.debug.print("residual={}", error)

        def true_fun(state):
            deps, b, eps, eps_t, epse_t, ep_t, Fn, K4, sig = state

            deps, iiter = conjugate_gradient(
                atol=1e-6,
                A=G_K_deps,
                b=b,
                additional=K4,
            )  # solve linear system using CG

            jax.debug.print("CG iteration {}", iiter)

            deps = deps.reshape(ndim, ndim, N, N)
            eps = jax.lax.add(eps, deps)  # update DOFs (array -> tensor.grid)
            sig, K4, epse, ep = constitutive(
                eps, eps_t, epse_t, ep_t
            )  # new residual stress
            b = -G(sig)  # compute residual

            return (deps, b, eps, eps_t, epse, ep, Fn, K4, sig)

        def false_fun(state):
            return state

        return jax.lax.cond(error > 1e-5, true_fun, false_fun, state), n

    # initialize: stress and strain tensor, and history
    eps = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    eps_t = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    epse_t = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    ep_t = jnp.zeros([N, N], dtype="float64")
    sig = jnp.zeros([ndim, ndim, N, N], dtype="float64")

    start_time = timeit.default_timer()

    # initial tangent operator: the elastic tangent
    K4 = K * II + 2.0 * μ * I4d

    # define incremental macroscopic strain
    ninc = 100

    DE = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    DE = DE.at[0, 0].set(jnp.sqrt(3.0) / 2.0 * epsbar / float(ninc))
    DE = DE.at[1, 1].set(-jnp.sqrt(3.0) / 2.0 * epsbar / float(ninc))

    b = -G_K_deps(DE, K4)
    eps = jax.lax.add(eps, DE)
    En = jnp.linalg.norm(eps)

    state = (DE, b, eps, eps_t, epse_t, ep_t, En, K4, sig)
    state = jax.device_put(state)

    final_state, xs = jax.lax.scan(solve_plasticity, init=state, xs=jnp.arange(0, 10))

    final_time = timeit.default_timer()
    return final_time - start_time


def use_ad_fft_method(structure):
    N = structure.shape[0]
    ndim = len(structure.shape)

    epsbar = 0.12

    # identity tensor (single tensor)
    i = jnp.eye(ndim)

    # identity tensors (grid)
    I = jnp.einsum(
        "ij,xy",
        i,
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )  # 2nd order Identity tensor
    I4 = jnp.einsum(
        "ijkl,xy->ijklxy",
        jnp.einsum("il,jk", i, i),
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )  # 4th order Identity tensor
    I4rt = jnp.einsum(
        "ijkl,xy->ijklxy",
        jnp.einsum("ik,jl", i, i),
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )
    I4s = (I4 + I4rt) / 2.0

    II = tensor.dyad22(I, I)
    I4d = I4s - II / 3.0

    # (inverse) Fourier transform (for each tensor component in each direction)
    @jax.jit
    def fft(x):
        return jnp.fft.fftshift(jnp.fft.fftn(jnp.fft.ifftshift(x), [N, N]))

    @jax.jit
    def ifft(x):
        return jnp.fft.fftshift(jnp.fft.ifftn(jnp.fft.ifftshift(x), [N, N]))

    Ghat4_2 = compute_Ghat_4_2(NN=(N,) * ndim, operator="fourier", length=1.0)

    # material parameters
    phase_constrast = 2

    K = param(structure, soft=0.833, hard=phase_constrast * 0.833)  # bulk      modulus
    μ = param(structure, soft=0.386, hard=phase_constrast * 0.386)  # shear     modulus
    H = param(
        structure, soft=2000.0e6 / 200.0e9, hard=phase_constrast * 2000.0e6 / 200.0e9
    )  # hardening modulus
    sigma_y = param(
        structure, soft=600.0e6 / 200.0e9, hard=phase_constrast * 600.0e6 / 200.0e9
    )  # initial yield stress

    n = 1.0

    @jax.jit
    def yield_function(ep):
        return sigma_y + H * ep**n

    @jax.jit
    def sigma(eps_t, epse_t, ep_t, eps):

        # elastic stiffness tensor
        C4e = K * II + 2.0 * μ * I4d

        # trial state
        epse_s = epse_t + (eps - eps_t)
        sig_s = tensor.ddot42(C4e, epse_s)
        sigm_s = tensor.ddot22(sig_s, I) / 3.0
        sigd_s = sig_s - sigm_s * I
        sigeq_s = jnp.sqrt(3.0 / 2.0 * tensor.ddot22(sigd_s, sigd_s))

        # avoid zero division below ("phi_s" is corrected below)
        Z = jnp.where(sigeq_s == 0, True, False)
        sigeq_s = jnp.where(Z == True, 1, sigeq_s)

        # evaluate yield surface, set to zero if elastic (or stress-free)
        sigy = yield_function(ep_t)
        phi_s = sigeq_s - sigy
        phi_s = 1.0 / 2.0 * (phi_s + jnp.abs(phi_s))
        phi_s = jnp.where(Z == True, 0.0, phi_s)
        elastic_pt = jnp.where(phi_s <= 0, True, False)

        # plastic multiplier, based on non-linear hardening
        # - initialize
        dep = phi_s / (3 * μ + H)

        # return map algorithm
        N = 3.0 / 2.0 * sigd_s / sigeq_s
        ep = ep_t + dep
        sig = sig_s - dep * N * 2.0 * μ
        epse = epse_s - dep * N
        # jax.debug.print('x = {}', sig_s)

        return sig, epse, ep

    # functions for the projection 'G', and the product 'G : K : eps'
    @jax.jit
    def G(A2):
        return jnp.real(ifft(tensor.ddot42(Ghat4_2, fft(A2)))).reshape(-1)

    @functools.partial(jax.jit, static_argnames=["sigma"])
    def G_P(deps, additional, sigma):
        eps_t, epse_t, ep_t, eps = additional
        deps = deps.reshape(ndim, ndim, N, N)
        primal, tangents = jax.jvp(
            functools.partial(sigma, eps_t, epse_t, ep_t), (eps,), (deps,)
        )
        return G(tangents[0])

    @functools.partial(jax.jit, static_argnames=["A", "K"])
    def conjugate_gradient(A, b, additional, K, atol=1e-5):

        b, additional = jax.device_put((b, additional))

        iiter = 0

        def true():
            return True

        def false():
            return False

        def body_fun(state):
            b, p, r, rsold, x, iiter = state
            Ap = A(p, additional, K)
            alpha = rsold / jnp.vdot(p, Ap)
            x = x + jnp.dot(alpha, p)
            r = r - jnp.dot(alpha, Ap)
            rsnew = jnp.vdot(r, r)
            p = r + (rsnew / rsold) * p
            rsold = rsnew
            iiter = iiter + 1
            return (b, p, r, rsold, x, iiter)

        def cond_fun(state):
            b, p, r, rsold, x, iiter = state
            return jax.lax.cond(
                jnp.logical_and(jnp.sqrt(rsold) > atol, iiter < 300), true, false
            )

        x = jnp.zeros_like(b)
        r = b - A(x, additional, K)
        p = r
        rsold = jnp.vdot(r, r)

        b, p, r, rsold, x, iiter = jax.lax.while_loop(
            cond_fun, body_fun, (b, p, r, rsold, x, iiter)
        )
        return x, iiter

    @jax.jit
    def solve_plasticity(state, n):
        deps, b, eps, eps_t, epse_t, ep_t, Fn, sig = state  # , K4 = state

        error = jnp.linalg.norm(deps) / Fn
        jax.debug.print("residual={}", error)

        def true_fun(state):
            deps, b, eps, eps_t, epse_t, ep_t, Fn, sig = state  # , K4 = state

            deps, iiter = conjugate_gradient(
                atol=1e-6,
                A=G_P,
                b=b,
                additional=(eps_t, epse_t, ep_t, eps),
                K=sigma,
            )  # solve linear system using CG

            deps = deps.reshape(ndim, ndim, N, N)
            eps = jax.lax.add(eps, deps)  # update DOFs (array -> tensor.grid)
            sig, epse, ep = sigma(eps_t, epse_t, ep_t, eps)
            b = -G(sig)  # compute residual

            jax.debug.print("CG iteration {}", iiter)

            return (deps, b, eps, eps_t, epse, ep, Fn, sig)  # , K4)

        def false_fun(state):
            return state

        return jax.lax.cond(error > 1e-5, true_fun, false_fun, state), n

    # initialize: stress and strain tensor, and history
    sig = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    eps = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    eps_t = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    epse_t = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    ep_t = jnp.zeros([N, N], dtype="float64")

    # define incremental macroscopic strain
    ninc = 100

    DE = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    DE = DE.at[0, 0].set(jnp.sqrt(3.0) / 2.0 * epsbar / float(ninc))
    DE = DE.at[1, 1].set(-jnp.sqrt(3.0) / 2.0 * epsbar / float(ninc))

    b = -G_P(DE, (eps_t, epse_t, ep_t, eps), sigma)
    eps = jax.lax.add(eps, DE)
    En = jnp.linalg.norm(eps)

    state = (DE, b, eps, eps_t, epse_t, ep_t, En, sig)
    state = jax.device_put(state)

    start_time = timeit.default_timer()

    final_state, xs = jax.lax.scan(solve_plasticity, init=state, xs=jnp.arange(0, 10))

    final_time = timeit.default_timer()
    return final_time - start_time


def use_linearize_ad_fft_method(structure):
    N = structure.shape[0]
    ndim = len(structure.shape)

    epsbar = 0.12

    # identity tensor (single tensor)
    i = jnp.eye(ndim)

    # identity tensors (grid)
    I = jnp.einsum(
        "ij,xy",
        i,
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )  # 2nd order Identity tensor
    I4 = jnp.einsum(
        "ijkl,xy->ijklxy",
        jnp.einsum("il,jk", i, i),
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )  # 4th order Identity tensor
    I4rt = jnp.einsum(
        "ijkl,xy->ijklxy",
        jnp.einsum("ik,jl", i, i),
        jnp.ones(
            [
                N,
            ]
            * ndim
        ),
    )
    I4s = (I4 + I4rt) / 2.0

    II = tensor.dyad22(I, I)
    I4d = I4s - II / 3.0

    # (inverse) Fourier transform (for each tensor component in each direction)
    @jax.jit
    def fft(x):
        return jnp.fft.fftshift(jnp.fft.fftn(jnp.fft.ifftshift(x), [N, N]))

    @jax.jit
    def ifft(x):
        return jnp.fft.fftshift(jnp.fft.ifftn(jnp.fft.ifftshift(x), [N, N]))

    Ghat4_2 = compute_Ghat_4_2(NN=(N,) * ndim, operator="fourier", length=1.0)

    # material parameters
    phase_constrast = 2

    K = param(structure, soft=0.833, hard=phase_constrast * 0.833)  # bulk      modulus
    μ = param(structure, soft=0.386, hard=phase_constrast * 0.386)  # shear     modulus
    H = param(
        structure, soft=2000.0e6 / 200.0e9, hard=phase_constrast * 2000.0e6 / 200.0e9
    )  # hardening modulus
    sigma_y = param(
        structure, soft=600.0e6 / 200.0e9, hard=phase_constrast * 600.0e6 / 200.0e9
    )  # initial yield stress

    n = 1.0

    @jax.jit
    def yield_function(ep):
        return sigma_y + H * ep**n

    @jax.jit
    def sigma(eps_t, epse_t, ep_t, eps):

        # elastic stiffness tensor
        C4e = K * II + 2.0 * μ * I4d

        # trial state
        epse_s = epse_t + (eps - eps_t)
        sig_s = tensor.ddot42(C4e, epse_s)
        sigm_s = tensor.ddot22(sig_s, I) / 3.0
        sigd_s = sig_s - sigm_s * I
        sigeq_s = jnp.sqrt(3.0 / 2.0 * tensor.ddot22(sigd_s, sigd_s))

        # avoid zero division below ("phi_s" is corrected below)
        Z = jnp.where(sigeq_s == 0, True, False)
        sigeq_s = jnp.where(Z == True, 1, sigeq_s)

        # evaluate yield surface, set to zero if elastic (or stress-free)
        sigy = yield_function(ep_t)
        phi_s = sigeq_s - sigy
        phi_s = 1.0 / 2.0 * (phi_s + jnp.abs(phi_s))
        phi_s = jnp.where(Z == True, 0.0, phi_s)
        elastic_pt = jnp.where(phi_s <= 0, True, False)

        # plastic multiplier, based on non-linear hardening
        # - initialize
        dep = phi_s / (3 * μ + H)

        # return map algorithm
        N = 3.0 / 2.0 * sigd_s / sigeq_s
        ep = ep_t + dep
        sig = sig_s - dep * N * 2.0 * μ
        epse = epse_s - dep * N
        # jax.debug.print('x = {}', sig_s)

        return sig, epse, ep

    # functions for the projection 'G', and the product 'G : K : eps'
    @jax.jit
    def G(A2):
        return jnp.real(ifft(tensor.ddot42(Ghat4_2, fft(A2)))).reshape(-1)

    @functools.partial(jax.jit, static_argnames=["jacobian"])
    def G_P(deps, additional, jacobian):
        # eps_t, epse_t, ep_t, eps = additional
        deps = deps.reshape(ndim, ndim, N, N)
        tangents = jacobian(deps)
        return G(tangents[0])

    @functools.partial(jax.jit, static_argnames=["A", "K"])
    def conjugate_gradient(A, b, additional, K, atol=1e-5):

        b, additional = jax.device_put((b, additional))

        iiter = 0

        def true():
            return True

        def false():
            return False

        def body_fun(state):
            b, p, r, rsold, x, iiter = state
            Ap = A(p, additional, K)
            alpha = rsold / jnp.vdot(p, Ap)
            x = x + jnp.dot(alpha, p)
            r = r - jnp.dot(alpha, Ap)
            rsnew = jnp.vdot(r, r)
            p = r + (rsnew / rsold) * p
            rsold = rsnew
            iiter = iiter + 1
            return (b, p, r, rsold, x, iiter)

        def cond_fun(state):
            b, p, r, rsold, x, iiter = state
            return jax.lax.cond(
                jnp.logical_and(jnp.sqrt(rsold) > atol, iiter < 300), true, false
            )

        x = jnp.zeros_like(b)
        r = b - A(x, additional, K)
        p = r
        rsold = jnp.vdot(r, r)

        b, p, r, rsold, x, iiter = jax.lax.while_loop(
            cond_fun, body_fun, (b, p, r, rsold, x, iiter)
        )
        return x, iiter

    @jax.jit
    def solve_plasticity(state, n):
        deps, b, eps, eps_t, epse_t, ep_t, Fn, sig = state

        error = jnp.linalg.norm(deps) / Fn
        jax.debug.print("residual={}", error)

        _, f_jvp = jax.linearize(functools.partial(sigma, eps_t, epse_t, ep_t), eps)

        def true_fun(state):
            deps, b, eps, eps_t, epse_t, ep_t, Fn, sig = state

            deps, iiter = conjugate_gradient(
                atol=1e-6,
                A=G_P,
                b=b,
                additional=(eps_t, epse_t, ep_t, eps),
                K=f_jvp,  # sigma,
            )  # solve linear system using CG

            deps = deps.reshape(ndim, ndim, N, N)
            eps = jax.lax.add(eps, deps)  # update DOFs (array -> tensor.grid)
            sig, epse, ep = sigma(eps_t, epse_t, ep_t, eps)
            b = -G(sig)  # compute residual

            jax.debug.print("CG iteration {}", iiter)

            return (deps, b, eps, eps_t, epse, ep, Fn, sig)  # , K4)

        def false_fun(state):
            return state

        return jax.lax.cond(error > 1e-5, true_fun, false_fun, state), n

    # initialize: stress and strain tensor, and history
    sig = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    eps = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    eps_t = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    epse_t = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    ep_t = jnp.zeros([N, N], dtype="float64")

    # define incremental macroscopic strain
    ninc = 100

    DE = jnp.zeros([ndim, ndim, N, N], dtype="float64")
    DE = DE.at[0, 0].set(jnp.sqrt(3.0) / 2.0 * epsbar / float(ninc))
    DE = DE.at[1, 1].set(-jnp.sqrt(3.0) / 2.0 * epsbar / float(ninc))

    _, f_jvp = jax.linearize(functools.partial(sigma, eps_t, epse_t, ep_t), eps)

    b = -G_P(DE, (eps_t, epse_t, ep_t, eps), f_jvp)

    eps = jax.lax.add(eps, DE)
    En = jnp.linalg.norm(eps)

    state = (DE, b, eps, eps_t, epse_t, ep_t, En, sig)
    state = jax.device_put(state)

    start_time = timeit.default_timer()

    final_state, xs = jax.lax.scan(solve_plasticity, init=state, xs=jnp.arange(0, 10))

    final_time = timeit.default_timer()
    return final_time - start_time


if __name__ == "__main__":
    # ------------------------------------------- ##
    # Input arguments
    # ------------------------------------------- ##
    parser = argparse.ArgumentParser()
    parser.add_argument("--profiling", type=str, default="time")
    parser.add_argument("--pixels", type=int, default=199)
    parser.add_argument("--kernel", type=str, default="standard")
    args = parser.parse_args()

    repeat = 2

    kernel = None
    if args.kernel == "standard":
        kernel = use_standard_fft_method
    elif args.kernel == "automatic":
        kernel = use_ad_fft_method
    elif args.kernel == "automatic-linearize":
        kernel = use_linearize_ad_fft_method

    if args.profiling == "time":

        timings_s = []
        n_range = [
            args.pixels,
        ] * repeat

        for i, n in enumerate(n_range):
            timings_s.append(kernel(setup(n)))

        filename = f"timing_elastoplastic_{args.kernel}.txt"
        data = np.stack((n_range, timings_s))

        with open(filename, "ab") as f:
            np.savetxt(f, data.T)

    elif args.profiling == "memory":
        structure = setup(args.pixels)
        filename = f"memory_elastoplastic_{args.kernel}.txt"

        with open(filename, "w") as f:
            print(memory_usage((kernel, (structure,)), interval=0.2), file=f)

    else:
        raise RuntimeError("profiling type doesnt exists", args.profiling)
