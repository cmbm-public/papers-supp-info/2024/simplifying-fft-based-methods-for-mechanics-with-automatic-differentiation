#!/bin/bash

#SBATCH --ntasks=1
#SBATCH --nodes=1
#SBATCH --cpus-per-task=2
#SBATCH --ntasks-per-node=1
#SBATCH --time=110:00:00
#SBATCH --job-name=scaling
#SBATCH --mem-per-cpu=16384
#SBATCH --tmp=50000
#SBATCH --output=/cluster/work/cmbm/mpundir/paper-repos/differentiable-fft-paper/tests/analysis.out
#SBATCH --error=/cluster/work/cmbm/mpundir/paper-repos/differentiable-fft-paper/tests/analysis.err

source /cluster/work/cmbm/local-stacks/stack-2024-06/load-scripts/load_fenicsx.sh
source /cluster/work/cmbm/mpundir/venv/my-venv/bin/activate

#python /cluster/work/cmbm/mpundir/paper-repos/differentiable-fft-paper/tests/hyperelasticity.py --pixels 127 --profiling time --kernel automatic

python /cluster/work/cmbm/mpundir/paper-repos/differentiable-fft-paper/tests/elastoplasticity.py --pixels 1023 --profiling time --kernel standard