import sys
import os
from memory_profiler import profile
from memory_profiler import memory_usage
import time

base_dir = os.path.join(os.path.dirname(__file__), "..")
sys.path.append(os.path.join(base_dir, "fft_helpers/"))

# jax related imports
import jax

jax.config.update("jax_compilation_cache_dir", "/cluster/scratch/mpundir/jax-cache")
import jax.numpy as jnp

jax.config.update("jax_enable_x64", True)  # use double-precision
jax.config.update("jax_platforms", "cpu")
import numpy as np

import functools
from jax.typing import ArrayLike
from jax import Array

from skimage.morphology import rectangle

# for profiling
import timeit
import tracemalloc

# from fft helpers
from projection_operators import compute_Ghat_4_2
import tensor_operators as tensor


import argparse
import perfplot


fp_standard =open('memory_profiler_standard.log','w+')
fp_automatic =open('memory_profiler_automatic.log','w+')
  


# material parameters + function to convert to grid of scalars
@functools.partial(jax.jit, static_argnames=["soft", "hard"])
def param(X, soft, hard):
    return soft * jnp.ones_like(X) * (X) + hard * jnp.ones_like(X) * (1 - X)


def setup(N):
    H, L = (N, N)
    r = int(N / 3)

    structure = np.zeros((H, L))                                                                                                                                                                                                                                                                                                                        
    structure[:r, -r:] += rectangle(r, r)

    return structure

#@profile(stream=fp_standard)                                                                                                                                                                                                                                                                                                                    
def use_standard_fft_method(structure):
    loading = 0.05

    N = structure.shape[0]
    ndim = len(structure.shape)

    # identity tensor (single tensor)
    i = jnp.eye(ndim)

    # identity tensors (grid)
    I = jnp.einsum("ij,xy", i, jnp.ones([N, N]))  # 2nd order Identity tensor
    I4 = jnp.einsum(
        "ijkl,xy->ijklxy", jnp.einsum("il,jk", i, i), jnp.ones([N, N])
    )  # 4th order Identity tensor
    I4rt = jnp.einsum("ijkl,xy->ijklxy", jnp.einsum("ik,jl", i, i), jnp.ones([N, N]))
    I4s = (I4 + I4rt) / 2.0

    II = tensor.dyad22(I, I)

    # material parameters
    elastic_modulus = {"hard": 5.7, "soft": 0.57}  # N/mm2
    poisson_modulus = {"hard": 0.386, "soft": 0.386}

    # lames constant
    lambda_modulus = {}
    shear_modulus = {}
    bulk_modulus = {}

    for key in elastic_modulus.keys():
        lambda_modulus[key] = (
            poisson_modulus[key]
            * elastic_modulus[key]
            / ((1 + poisson_modulus[key]) * (1 - 2 * poisson_modulus[key]))
        )

        shear_modulus[key] = elastic_modulus[key] / (2 * (1 + poisson_modulus[key]))

        bulk_modulus[key] = lambda_modulus[key] + 2 * shear_modulus[key] / 3

    # material parameters
    K = param(
        structure, soft=bulk_modulus["soft"], hard=bulk_modulus["hard"]
    )  # bulk      modulus
    mu = param(
        structure, soft=shear_modulus["soft"], hard=shear_modulus["hard"]
    )  # shear     modulus

    Ghat4_2 = compute_Ghat_4_2(NN=(N,) * ndim, operator="fourier", length=1.0)

    # (inverse) Fourier transform (for each tensor component in each direction)
    @jax.jit
    def fft(x):
        return jnp.fft.fftshift(jnp.fft.fftn(jnp.fft.ifftshift(x), [N, N]))

    @jax.jit
    def ifft(x):
        return jnp.fft.fftshift(jnp.fft.ifftn(jnp.fft.ifftshift(x), [N, N]))

    # functions for the projection 'G', and the product 'G : K : dF'
    @jax.jit
    def G(A2):
        return jnp.real(ifft(tensor.ddot42(Ghat4_2, fft(A2)))).reshape(-1)

    @jax.jit
    def K_dF(dF, K4):
        # jax.debug.print('x={}', K4)
        return tensor.trans2(
            tensor.ddot42(K4, tensor.trans2(dF.reshape(ndim, ndim, N, N)))
        )

    @jax.jit
    def G_K_dF(dF, K4):
        return G(K_dF(dF, K4))

    # constitutive model: grid of "F" -> grid of "P", "K4"        [grid of tensors]
    # @jax.jit
    def constitutive(F):
        C4 = K * II + 2.0 * mu * (I4s - 1.0 / 3.0 * II)
        S = tensor.ddot42(C4, 0.5 * (tensor.dot22(tensor.trans2(F), F) - I))
        P = tensor.dot22(F, S)
        K4 = tensor.dot24(S, I4) + tensor.ddot44(
            tensor.ddot44(I4rt, tensor.dot42(tensor.dot24(F, C4), tensor.trans2(F))),
            I4rt,
        )
        return P, K4

    @functools.partial(jax.jit, static_argnums=(0,))
    def conjgrad(A, b, K4, atol=1e-5):

        b, K4 = jax.device_put((b, K4))

        iiter = 0

        def body_fun(state):
            b, p, r, rsold, x, iiter = state
            Ap = A(p, K4)
            alpha = rsold / jnp.vdot(p, Ap)
            x = x + jnp.dot(alpha, p)
            r = r - jnp.dot(alpha, Ap)
            rsnew = jnp.vdot(r, r)
            p = r + (rsnew / rsold) * p
            rsold = rsnew
            iiter += 1
            return (b, p, r, rsold, x, iiter)

        def cond_fun(state):
            b, p, r, rsold, x, iiter = state
            return jnp.sqrt(rsold) > atol

        x = jnp.zeros_like(b)
        r = b - A(x, K4)
        p = r
        rsold = jnp.vdot(r, r)  # jnp.dot(jnp.transpose(r), r)

        b, p, r, rsold, x, iiter = jax.lax.while_loop(
            cond_fun, body_fun, (b, p, r, rsold, x, iiter)
        )
        return x, iiter

    @jax.jit
    def solve_netwon_raphson(state, n):
        dF, b, F, Fn, K4, iiter = state

        error = jnp.linalg.norm(dF) / Fn
        jax.debug.print("residual={}", error)

        def true_fun(state):
            dF, b, F, Fn, K4, iiter = state

            dF, iiter_cg = conjgrad(
                atol=1e-8,
                A=G_K_dF,
                b=b,
                K4=K4,
            )  # solve linear system using CG

            dF = dF.reshape(ndim, ndim, N, N)
            F = jax.lax.add(F, dF)  # update DOFs (array -> tensor.grid)
            P, K4 = constitutive(F)  # new residual stress
            b = -G(P)  # convert residual stress to residual
            iiter = iiter.at[n, 0].set(iiter_cg)
            iiter = iiter.at[n, 1].set(jnp.linalg.norm(dF) / Fn)

            return (dF, b, F, Fn, K4, iiter)

        def false_fun(state):
            dF, b, F, Fn, K4, iiter = state

            iiter = iiter.at[n, 1].set(jnp.linalg.norm(dF) / Fn)

            return (dF, b, F, Fn, K4, iiter)

        return jax.lax.cond(error > 1e-6, true_fun, false_fun, state), n

    # set macroscopic loading
    DbarF = jnp.zeros([ndim, ndim, N, N])
    DbarF = DbarF.at[0, 1].add(loading)

    # initial residual: distribute "barF" over grid using "K4"
    F = jnp.array(I, copy=True)
    P, K4 = constitutive(F)
    b = -G_K_dF(DbarF, K4)
    F = jax.lax.add(F, DbarF)
    Fn = jnp.linalg.norm(F)
    # starting the monitoring
    #tracemalloc.start()
    start_time = timeit.default_timer()
    P, K4 = constitutive(F)
    iiter = jnp.zeros((10, 2))
    state = (DbarF, b, F, Fn, K4, iiter)
    initial_state = jax.device_put(state)
    final_state, xs = jax.lax.scan(
        solve_netwon_raphson, init=initial_state, xs=jnp.arange(0, 10)
    )
    #memory_usage = tracemalloc.get_traced_memory()[1]
    # displaying the memory
    final_time = timeit.default_timer()
    print(final_time-start_time)

        # stopping the library
        #tracemalloc.stop()

    return final_time - start_time


#@profile(stream=fp_automatic)
def use_ad_fft_method(structure):
    loading = 0.05
    N = structure.shape[0]
    ndim = len(structure.shape)

    # identity tensor (single tensor)
    i = jnp.eye(ndim)

    # identity tensors (grid)
    I = jnp.einsum("ij,xy", i, jnp.ones([N, N]))  # 2nd order Identity tensor
    I4 = jnp.einsum(
        "ijkl,xy->ijklxy", jnp.einsum("il,jk", i, i), jnp.ones([N, N])
    )  # 4th order Identity tensor
    I4rt = jnp.einsum("ijkl,xy->ijklxy", jnp.einsum("ik,jl", i, i), jnp.ones([N, N]))
    I4s = (I4 + I4rt) / 2.0

    II = tensor.dyad22(I, I)

    # material parameters
    elastic_modulus = {"hard": 5.7, "soft": 0.57}  # N/mm2
    poisson_modulus = {"hard": 0.386, "soft": 0.386}

    # lames constant
    lambda_modulus = {}
    shear_modulus = {}
    bulk_modulus = {}

    for key in elastic_modulus.keys():
        lambda_modulus[key] = (
            poisson_modulus[key]
            * elastic_modulus[key]
            / ((1 + poisson_modulus[key]) * (1 - 2 * poisson_modulus[key]))
        )

        shear_modulus[key] = elastic_modulus[key] / (2 * (1 + poisson_modulus[key]))

        bulk_modulus[key] = lambda_modulus[key] + 2 * shear_modulus[key] / 3

    # material parameters
    K = param(
        structure, soft=bulk_modulus["soft"], hard=bulk_modulus["hard"]
    )  # bulk      modulus
    μ0 = param(
        structure, soft=shear_modulus["soft"], hard=shear_modulus["hard"]
    )  # shear     modulus
    λ0 = param(
        structure, soft=lambda_modulus["soft"], hard=lambda_modulus["hard"]
    )  # shear     modulus

    Ghat4_2 = compute_Ghat_4_2(NN=(N,) * ndim, operator="fourier", length=1.0)

    # (inverse) Fourier transform (for each tensor component in each direction)
    @jax.jit
    def fft(x):
        return jnp.fft.fftshift(jnp.fft.fftn(jnp.fft.ifftshift(x), [N, N]))

    @jax.jit
    def ifft(x):
        return jnp.fft.fftshift(jnp.fft.ifftn(jnp.fft.ifftshift(x), [N, N]))

    # functions for the projection 'G', and the product 'G : K : dF'
    @jax.jit
    def G(A2):
        return jnp.real(ifft(tensor.ddot42(Ghat4_2, fft(A2)))).reshape(-1)

    @jax.jit
    def green_lagrange_strain(F: ArrayLike) -> Array:
        return 0.5 * (tensor.dot22(tensor.trans2(F), F) - I)

    @jax.jit
    def strain_energy(F: ArrayLike) -> Array:
        E = green_lagrange_strain(F)
        E = 0.5 * (E + tensor.trans2(E))
        energy = 0.5 * jnp.multiply(λ0, tensor.trace2(E) ** 2) + jnp.multiply(
            μ0, tensor.trace2(tensor.dot22(E, E))
        )
        return energy.sum()

    piola_kirchhoff = jax.jit(jax.jacrev(strain_energy))

    @functools.partial(jax.jit, static_argnames=["piola_kirchhoff"])
    def G_P(dF, additional, piola_kirchhoff):
        dF = dF.reshape(ndim, ndim, N, N)
        tangents = jax.jvp(piola_kirchhoff, (additional,), (dF,))[1]
        return G(tangents)

    @functools.partial(jax.jit, static_argnames=["A", "K"])
    def conjugate_gradient(A, b, additional, K, atol=1e-5):
        b, additional = jax.device_put((b, additional))
        iiter = 0

        def body_fun(state):
            b, p, r, rsold, x, iiter = state
            Ap = A(p, additional, K)
            alpha = rsold / jnp.vdot(p, Ap)
            x = x + jnp.dot(alpha, p)
            r = r - jnp.dot(alpha, Ap)
            rsnew = jnp.vdot(r, r)
            p = r + (rsnew / rsold) * p
            rsold = rsnew
            iiter += 1
            return (b, p, r, rsold, x, iiter)

        def cond_fun(state):
            b, p, r, rsold, x, iiter = state
            return jnp.sqrt(rsold) > atol

        x = jnp.zeros_like(b)
        r = b - A(x, additional, K)
        p = r
        rsold = jnp.vdot(r, r)
        b, p, r, rsold, x, iiter = jax.lax.while_loop(
            cond_fun, body_fun, (b, p, r, rsold, x, iiter)
        )
        return x, iiter

    @jax.jit
    def solve_netwon_raphson(state, n):
        dF, b, F, Fn, iiter = state

        error = jnp.linalg.norm(dF) / Fn
        jax.debug.print("residual={}", error)

        def true_fun(state):
            dF, b, F, Fn, iiter = state

            dF, iiter_cg = conjugate_gradient(
                atol=1e-8,
                A=G_P,
                b=b,
                additional=F,
                K=piola_kirchhoff,
            )  # solve linear system using CG

            dF = dF.reshape(ndim, ndim, N, N)
            F = jax.lax.add(F, dF)  # update DOFs (array -> tensor.grid)
            P = piola_kirchhoff(F)  # new residual stress
            b = -G(P)  # convert residual stress to residual
            iiter = iiter.at[n, 0].set(iiter_cg)
            iiter = iiter.at[n, 1].set(jnp.linalg.norm(dF) / Fn)

            return (dF, b, F, Fn, iiter)

        def false_fun(state):
            dF, b, F, Fn, iiter = state

            iiter = iiter.at[n, 1].set(jnp.linalg.norm(dF) / Fn)

            return (dF, b, F, Fn, iiter)

        return jax.lax.cond(error > 1e-6, true_fun, false_fun, state), n

    # set macroscopic loading
    DbarF = jnp.zeros([ndim, ndim, N, N])
    DbarF = DbarF.at[0, 1].add(loading)

    # initial residual: distribute "barF" over grid using "K"
    F = jnp.array(I, copy=True)
    #_ = piola_kirchhoff(F)

    b = -G_P(DbarF, F, piola_kirchhoff)
    F = jax.lax.add(F, DbarF)
    Fn = jnp.linalg.norm(F)
    # starting the monitoring
    #tracemalloc.start()
    
    iiter = jnp.zeros((10, 2))
    state = (DbarF, b, F, Fn, iiter)
    initial_state = jax.device_put(state)
    # starting the monitoring
    start_time = timeit.default_timer()
    final_state, xs = jax.lax.scan(
        solve_netwon_raphson, init=initial_state, xs=jnp.arange(0, 10)
    )
    #memory_usage = tracemalloc.get_traced_memory()[1]
    # displaying the memory
    final_time = timeit.default_timer()
    
    return final_time - start_time

        # stopping the library
        #tracemalloc.stop()

    #return final_time - start_time, memory_usage


if __name__ == "__main__":
    # ------------------------------------------- ##
    # Input arguments
    # ------------------------------------------- ##
    parser = argparse.ArgumentParser()
    parser.add_argument("--profiling", type=str, default="time")
    parser.add_argument("--pixels", type=int, default=199)
    parser.add_argument("--kernel", type=str, default="standard")
    args = parser.parse_args()

    repeat = 2

    kernel = None
    if args.kernel == "standard":
        kernel = use_standard_fft_method
    elif args.kernel == "automatic":
        kernel = use_ad_fft_method

    if args.profiling == "time":

        timings_s = []
        n_range = [
            args.pixels,
        ] * repeat

        for i, n in enumerate(n_range):
            timings_s.append(kernel(setup(n)))

        filename = f"timing_hyperelastic_{args.kernel}.txt"
        data = np.stack((n_range, timings_s))

        with open(filename, "ab") as f: 
            np.savetxt(f, data.T)

    elif args.profiling == "memory":
        structure = setup(args.pixels)
        filename = f"memory_hyperelastic_{args.kernel}.txt"

        with open(filename, "w") as f:
            print(memory_usage((kernel, (structure,)), interval=0.2), file=f)

    else:
        raise RuntimeError("profiling type doesnt exists", args.profiling)


    #parser = argparse.ArgumentParser()
    #parser.add_argument("--profiling", type=str, default='time')
    #parser.add_argument("--pixels", type=int, default=127)
    #parser.add_argument("--kernel", type=str, default="standard")
    #args = parser.parse_args()

    """if args.implementation == 'Standard':
        time, memory = use_standard_fft_method(args.pixels)
    elif args.implementation == 'Automatic':
        time, memory = use_ad_fft_method(args.pixels)
    else:
        raise RuntimeError('Implmentation for defined', args.implementation)"""

    # store the data
    # time, memory = use_standard_fft_method(args.pixels)
    # print(time, memory)
    # time, memory = use_ad_fft_method(args.pixels)
    # print(time, memory)

    '''factor = 1

    kernel = None
    if args.kernel == 'standard':
        kernel = use_standard_fft_method
    elif args.kernel == 'automatic':
        kernel = use_ad_fft_method
        factor = 4

    if args.profiling == 'time':


        b = perfplot.bench(
            setup=setup,
            kernels=[kernel],
            n_range = [args.pixels,]*factor,
            #n_range=[2**k-1 for k in range(5,10)],
            equality_check=None,
            xlabel="system size",
        )

        
        
        print(b.n_range)
        print(b.timings_s)
        print(b.labels)

        for i, label in enumerate(b.labels):
            filename = f"time_profile_{args.kernel}_{b.n_range[0]}.txt"
            data = np.stack((b.n_range, b.timings_s[i]))
            #np.savetxt(filename, data)
            with open(filename, "ab") as f:
                f.write(b"\n")  
                np.savetxt(f, data.T)


    elif args.profiling == 'memory':
        structure, loading = setup(511)

        use_standard_fft_method(structure, loading)
        use_ad_fft_method(structure, loading)

    else:
        raise RuntimeError('profiling type doesnt exists', args.profiling)'''


    #memory_usage((use_standard_fft_method, (structure, loading)), interval=0.01,  stream=fp_standard)

    #memory_usage((use_ad_fft_method, (structure, loading)), interval=0.01,  stream=fp_automatic)