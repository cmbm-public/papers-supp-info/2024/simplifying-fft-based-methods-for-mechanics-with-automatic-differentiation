# Differentiable FFT-based method (Supplementary Code)

The repository contains the code for AD-enhanced FFT-based method for solid mechanics. It is the supplementary code to the paper `Simplifying FFT-based methods for solid mechanics with automatic differentiation`. The code has been provided in the form of `Jupyter` notebooks that are independent of each other cand thus can be run individually. The notebooks contain in-depth explanation of the use of `JAX` library to make the FFT-based method differentiable.  

### Paper Title
Simplifying FFT-based methods for solid mechanics with automatic differentiation

### Authors
Mohit Pundir (mpundir@ethz.ch), David S. Kammer (dkammer@ethz.ch)

### Date
2024/08/06

### Requirements
To run the FFT-based code, the following Python libraries are required:
- JAX
- scikit-image
- numpy
- matplotlib

The above python libraries can be installed via PyPi (`pip`).

To run the FE-FFT based code, please also install the FEniCSx (v0.8.0). 

### Notebooks
- File **notebooks/autodiff_st_venant.ipynb** contains the automatic derivation of stress and tangent operator for St. Venant-Kirchhoff material and it reproduces the Figure 1 of the paper. 
- File **notebooks/autodiff_elastoplasticity.ipynb**  contains the AD-enhances implementation of `J2` plasticity. It reproduces the Figure 2 of the paper.
- File **notebooks/autodiff_architected.ipynb**  contains the implementation for linear elastic two-phase architected material. It reproduces the Figure 3 of the paper.
- File **notebooks/autodiff_multiscale_lattice.ipynb**  contians FE-FFT implementation using `FEniCSx` and `AD-enhanced FFT` method for a hyperelastic lattice-shaped RVE. It reproduces the Figure 4 of the paper. 
- File **notebooks/analytical_st_venant.ipynb** contains the analytical implementation of stress and tangent operator for St. Venant-Kirchhoff material and produces the output that is used in **notebooks/autodiff_st_venant.ipynb** for comparison.
- File **notebooks/analytical_elsotplasticity.ipynb** contains the analytical implementation of tangent operator for `J2` plasticity and produces the output that is used in **notebooks/autodiff_elsotplasticity.ipynb** for comparison.
- File **notebooks/uncertainity_quantification.ipynb** contains the automatic differentiation implementaiton of propagationg uncertainities.
- File **notebooks/topo_linear.ipynb** contains the AD computation of sensitivities for a FFT-based toplogy optimization.

### Tests
The folder contain various benchmarking, scaling analysis and testing notebooks/scripts.
- File **analysis_scaling.ipynb** reproduces the time scaling and memory usuage comparison for a AD-enhanced FFT method and standard FFT method.
- Scripts **hyperelasticity.py** and **elastoplasticity.py** is the scripts to produce scaling data.
- File **efficiency_test.ipynb** tests the computaitonal time for different AD modes and of jvp and linearize function.
- File **homogenized_properties_test.ipynb** compares the AD-derived homogenized properties with Eshelby analytical solution.