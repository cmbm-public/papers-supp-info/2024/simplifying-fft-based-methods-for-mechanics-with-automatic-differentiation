import jax

jax.config.update("jax_compilation_cache_dir", "/cluster/scratch/mpundir/jax-cache")
import jax.numpy as jnp

jax.config.update("jax_enable_x64", True)  # use double-precision
jax.config.update("jax_platforms", "cpu")
import numpy as np
import functools

import itertools


def monte_carlo(random_variables=None, has_aux=False):
    def decorator_func(func):
        @functools.wraps(func)
        def wrapper_func(*args, **kwargs):
            output_variables = []
            aux_values = []
            for variable in random_variables:
                if has_aux:
                    output, aux_value = func(variable, **kwargs)
                    aux_values.append(aux_value)
                else:
                    output = func(variable, **kwargs)

                output_variables.append(output)

            return (output_variables, aux_values) if has_aux else output_variables

        return wrapper_func


    return decorator_func
