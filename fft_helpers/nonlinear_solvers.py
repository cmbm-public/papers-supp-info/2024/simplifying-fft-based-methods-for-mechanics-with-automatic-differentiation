from dolfinx import fem
from petsc4py import PETSc
from dolfinx.fem.petsc import LinearProblem
import ufl
import numpy as np

# Based on https://bleyerj.github.io/comet-fenicsx/tours/nonlinear_problems/plasticity/plasticity.html
class CustomTangentProblem(fem.petsc.LinearProblem):
    def assemble_rhs(self, u=None):
        """Assemble right-hand side and lift Dirichlet bcs.

        Parameters
        ----------
        u : dolfinx.fem.Function, optional
            For non-zero Dirichlet bcs u_D, use this function to assemble rhs with the value u_D - u_{bc}
            where u_{bc} is the value of the given u at the corresponding. Typically used for custom Newton methods
            with non-zero Dirichlet bcs.
        """

        # Assemble rhs
        with self._b.localForm() as b_loc:
            b_loc.set(0)
        fem.petsc.assemble_vector(self._b, self._L)

        # Apply boundary conditions to the rhs
        x0 = [] if u is None else [u.vector]
        fem.petsc.apply_lifting(self._b, [self._a], bcs=[self.bcs], x0=x0, scale=1.0)
        self._b.ghostUpdate(addv=PETSc.InsertMode.ADD, mode=PETSc.ScatterMode.REVERSE)
        x0 = None if u is None else u.vector
        fem.petsc.set_bc(self._b, self.bcs, x0, scale=1.0)

    def assemble_lhs(self):
        self._A.zeroEntries()
        fem.petsc.assemble_matrix_mat(self._A, self._a, bcs=self.bcs)
        self._A.assemble()

    def solve_system(self):
        # Solve linear system and update ghost values in the solution
        self._solver.solve(self._b, self._x)
        self.u.x.scatter_forward()

# problem should be a NonlinearVariationalProblem

class CustomNonlinearProblem:
    def __init__(self, res, u, bcs, jac):
        self.res = res
        self.u = u
        self.bcs = bcs
        self.jac = jac
        

        

class CustomNonlinearSolver:

    # bcs: original object
    def __init__(self, problem, callbacks = [], u0_satisfybc = False): 
        
        self.problem = problem
        self.callbacks = callbacks
        self.u0_satisfybc = u0_satisfybc
    
        self.du = fem.Function(self.problem.u.function_space)
 
        self.tangent_problem = CustomTangentProblem(
        self.problem.jac, -self.problem.res,
        u=self.du,
        bcs=self.problem.bcs,
        petsc_options={
            "ksp_type": "preonly",
            "pc_type": "lu",
            "pc_factor_mat_solver_type": "mumps"})
        
    def reset_bcs(self, bcs):
        self.problem.reset_bcs(bcs)
        
    def solve(self, Nitermax = 10, tol = 1e-8, report = False):
        # compute the residual norm at the beginning of the load step
        self.call_callbacks()
        nRes = []
        
        self.tangent_problem.assemble_rhs()
        nRes.append(self.tangent_problem._b.norm())
        if(nRes[0]<tol): 
            nRes[0] = 1.0
        self.du.x.array[:] = 0.0

        niter = 0
        while nRes[niter] / nRes[0] > tol and niter < Nitermax:
            # solve for the displacement correction
            self.tangent_problem.assemble_lhs()
            self.tangent_problem.solve_system()

            # update the displacement increment with the current correction
            self.problem.u.vector.axpy(1, self.du.vector)  # Du = Du + 1*du
            self.problem.u.x.scatter_forward()
            self.call_callbacks()
            
            self.tangent_problem.assemble_rhs()
            nRes.append(self.tangent_problem._b.norm())
            
            niter += 1
            if(report):
                print(" Residual:", nRes[-1]/nRes[0])

        return nRes

    def call_callbacks(self):
        [foo(self.problem.u, self.du) for foo in self.callbacks]