import jax
jax.config.update("jax_compilation_cache_dir", "/cluster/scratch/mpundir/jax-cache")
import jax.numpy as jnp
jax.config.update("jax_enable_x64", True)  # use double-precision
jax.config.update('jax_platforms', 'cpu')
import numpy as np
import functools

import itertools



def finite_difference_vector(increment):
    def decorator_func(func):
        @functools.wraps(func)
        def wrapper_func(*args, **kwargs):
            fixed_value = func(*args, **kwargs)
            ndim = len(fixed_value)
            output = np.zeros((ndim, ndim ))
            for i, j in itertools.product(range(ndim), repeat=2):
                F_kl  = args[0] + increment
                output[i, j] = (func(F_kl)[j] - fixed_value[j]) / increment
            return fixed_value, output
        return wrapper_func
    return decorator_func


def finite_difference(increment, ndim=2, has_aux=False):
    def decorator_func(func):
        @functools.wraps(func)
        def wrapper_func(*args, **kwargs):
            # Call the function once with unperturbed arguments
            if has_aux:
                fixed_value, aux_values = func(*args, **kwargs)
            else:
                fixed_value = func(*args, **kwargs)

            # Preallocate the output array
            output = np.zeros((ndim,) * 4)

            # Precompute the deformation gradients
            delta_F = np.array([[compute_perturb_deformation_gradient(increment, k, l)
                                 for l in range(ndim)] for k in range(ndim)])

            # Iterate over i, j, k, l in a vectorized way
            for i, j, k, l in itertools.product(range(ndim), repeat=4):
                F_kl = args[0] + delta_F[k, l]
                # Evaluate the function with the perturbed F_kl
                func_F_kl = func(F_kl)[0] if has_aux else func(F_kl)
                # Compute finite difference and store result
                output[i, j, k, l] = (func_F_kl[i, j] - fixed_value[i, j]) / delta_F[k, l][k, l]

            return (output, aux_values) if has_aux else output

        return wrapper_func
    return decorator_func


def finite_difference_grid(increment,  ndim=2):
    def decorator_func(func):
        @functools.wraps(func)
        def wrapper_func(*args, **kwargs):
            fixed_value = func(*args, **kwargs)
            N  = fixed_value.shape[-1]
            output = jnp.zeros((ndim,)*4 + (N,) * ndim)
            for i, j, k, l in itertools.product(range(ndim), repeat=4):
                ΔF_kl_i = compute_perturb_deformation_gradient(increment, k, l)
                ΔF_kl = jnp.einsum(
                "ij,xy", ΔF_kl_i, jnp.ones([N, N])
                )
                F_kl  = args[0] + ΔF_kl
                output = output.at[i, j, k, l].set( (func(F_kl).at[i, j].get() - fixed_value.at[i, j].get()) / ΔF_kl.at[k, l].get() )
            return output
        return wrapper_func
    return decorator_func


def compute_perturb_deformation_gradient(ΔF, k, l):
    basis_vectors = [[1, 0], [0, 1]]
    ΔF_kl = ΔF * np.outer(basis_vectors[k], basis_vectors[l])
    return ΔF_kl