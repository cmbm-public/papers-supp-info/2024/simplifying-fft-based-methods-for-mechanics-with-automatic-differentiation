import numpy as np

def upscale_binary_array(arr):
    # Double the size of the array by repeating elements
    return np.repeat(np.repeat(arr, 2, axis=0), 2, axis=1)


def downscale_binary_array(arr):
    # Ensure the input is a 2N x 2N array
    assert arr.shape[0] % 2 == 0 and arr.shape[1] % 2 == 0, "Array must have even dimensions"

    # Reshape the array into 2x2 blocks and compute the mean across each block
    N = arr.shape[0] // 2
    reshaped = arr.reshape(N, 2, N, 2)  # Reshape into blocks of 2x2
    return reshaped.mean(axis=(1, 3))  # Average over the blocks