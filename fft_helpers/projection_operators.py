import jax
jax.config.update("jax_compilation_cache_dir", "/cluster/scratch/mpundir/jax-cache")
import jax.numpy as jnp
jax.config.update("jax_enable_x64", True)  # use double-precision
jax.config.update('jax_platforms', 'cpu')
import numpy as np
import functools

import itertools


@functools.partial(jax.jit, static_argnames=['NN', 'length', 'operator'])
def compute_Ghat_4_2(NN, length=1, operator='forward-difference'):
    ndim   = len(NN)
    delta_space = length/NN[0]

    
    Ghat4_2 = np.zeros((ndim,ndim, ndim, ndim)+ NN, dtype='complex') # zero initialize
    freq = [np.arange(-(NN[ii]-1)/2.,+(NN[ii]+1)/2.)/length for ii in range(ndim)] 
    delta   = lambda i,j: float(i==j)               # Dirac delta function

    for i,j,l,m in itertools.product(range(ndim),repeat=4):
        for ind in itertools.product(*[range(n) for n in NN]):
            q = np.empty(ndim, dtype='complex')
            Dop = np.empty(ndim, dtype='complex')

            factor = 1.
            for jj in range(ndim):
                factor *=  0.5 * (1 + np.exp(1j * 2*np.pi*freq[jj][ind[jj]] * delta_space))
            
            for ii in range(ndim):
                q[ii] = 2*np.pi*freq[ii][ind[ii]]   ## frequency vector # 2*pi*(n)/samplingspace/n https://arxiv.org/pdf/1412.8398  
                
                if operator == 'fourier':
                    Dop[ii] = 1j*q[ii]                  ## [fourier operator]
                elif operator == 'forward-difference':
                    Dop[ii] = (np.exp(1j*q[ii]*delta_space)-1)/delta_space             
                elif operator == 'central-difference':
                    Dop[ii] = 1j*np.sin(q[ii]*delta_space)/delta_space    
                elif operator == '4-order-cd':
                    Dop[ii] = 1j*(8*np.sin(q[ii]*delta_space)/(6*delta_space) - np.sin(2*q[ii]*delta_space)/(6*delta_space))    ## [fourth-order central difference operator]
                elif operator == '8-order-cd':
                    Dop[ii] = 1j*(8*np.sin(q[ii]*delta_space)/(5*delta_space) - 2*np.sin(2*q[ii]*delta_space)/(5*delta_space) + 8*np.sin(3*q[ii]*delta_space)/(105*delta_space) - np.sin(4*q[ii]*delta_space)/(140*delta_space) ) 

                elif operator == 'rotated':
                    Dop[ii] = ( 2 * 1j * np.tan(q[ii] * delta_space / 2) * factor / delta_space )
              
            
            if not Dop.dot(np.conjugate(Dop)) == 0:          # zero freq. -> mean
                Dop_inverse = np.conjugate(Dop)/(Dop.dot(np.conjugate(Dop)))
                Ghat4_2[i,j,l,m][ind] = delta(i,m)*Dop[j]*Dop_inverse[l]
    return Ghat4_2


@functools.partial(jax.jit, static_argnames=['N', 'length', 'operator'])
def compute_Ghat_2_1(N, length=1, operator='forward-difference'):
    ndim = len(N)
    delta_space = length/N[0]


    # PROJECTION IN FOURIER SPACE #############################################
    Ghat2_1 = np.zeros((ndim,ndim)+N, dtype='complex') # zero initialize
    freq = [np.arange(-(N[ii]-1)/2.,+(N[ii]+1)/2.)/length for ii in range(ndim)]
      
    for i,j in itertools.product(range(ndim),repeat=2):
        for ind in itertools.product(*[range(n) for n in N]):
            q = np.empty(ndim, dtype='complex')
            Dop = np.empty(ndim, dtype='complex')
            for ii in range(ndim):
                q[ii] = 2*np.pi*freq[ii][ind[ii]] ## frequency vector
                if operator == 'fourier':
                    Dop[ii] = 1j*q[ii]                                   
                elif operator == 'central-difference':
                    Dop[ii] = 1j*np.sin(q[ii]*delta_space)/delta_space   
                elif operator == '4-order-cd':
                    Dop[ii] = 1j*(8*np.sin(q[ii]*delta_space)/(6*delta_space) - np.sin(2*q[ii]*delta_space)/(6*delta_space))   
                elif operator == '8-order-cd':
                    Dop[ii] = 1j*(8*np.sin(q[ii]*delta_space)/(5*delta_space) - 2*np.sin(2*q[ii]*delta_space)/(5*delta_space) + 8*np.sin(3*q[ii]*delta_space)/(105*delta_space) - np.sin(4*q[ii]*delta_space)/(140*delta_space) ) 
                elif operator == 'forward-difference':
                    Dop[ii] = (np.exp(1j*q[ii]*delta_space)-1)/delta_space
                else:
                    raise RuntimeError('operator incorrectly defined')
        
            if not Dop.dot(np.conjugate(Dop)) == 0:          # zero freq. -> mean
                Dop_inverse = np.conjugate(Dop)/(Dop.dot(np.conjugate(Dop)))
                Ghat2_1[i,j][ind] = Dop[i]*Dop_inverse[j]

    return Ghat2_1