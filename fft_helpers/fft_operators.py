import numpy as np
import tensor_operators as tensor


def laplacian_operator(N, ndim, length=1.):
    delta_space = length/N
    x_2 = np.zeros([ndim, N, N], dtype="int64")  # position vectors
    q_2 = np.zeros([ndim, N, N], dtype="int64")  # frequency vectors
    Dq_2 = np.zeros([ndim, N, N], dtype="complex")  # frequency vectors

    # - set "x_2" as position vector of all grid-points
    x_2[0], x_2[1] = np.mgrid[:N, :N]

    shape = [N, N]  # number of voxels in all directions
    # - convert positions "x_2" for frequencies "q_2"
    for i in range(ndim):
        freq = np.arange(-(shape[i] - 1) / 2, +(shape[i] + 1) / 2, dtype="int64")/length
        q_2[i] = 2 * np.pi * freq[x_2[i]]  #2*pi*(n)/samplingspace/n https://arxiv.org/pdf/1412.8398
        Dq_2[i] = 1j * q_2[i]

    # computing laplacian operator
    Q = tensor.dot11(Dq_2, Dq_2)
    return Q


def gradient_operator(N, ndim, length=1.):
    delta_space = length/N

    x_2 = np.zeros([ndim, N, N], dtype="int64")  # position vectors
    q_2 = np.zeros([ndim, N, N], dtype="int64")  # frequency vectors
    Dq_2 = np.zeros([ndim, N, N], dtype="complex")  # frequency vectors

    # - set "x_2" as position vector of all grid-points
    x_2[0], x_2[1] = np.mgrid[:N, :N]

    shape = [N, N]  # number of voxels in all directions
    # - convert positions "x_2" for frequencies "q_2"
    for i in range(ndim):
        freq = np.arange(-(shape[i] - 1) / 2, +(shape[i] + 1) / 2, dtype="int64")/length
        q_2[i] = 2 * np.pi * freq[x_2[i]] # 2*pi*(n)/samplingspace/n https://arxiv.org/pdf/1412.8398
        Dq_2[i] = 1j * q_2[i]

    return Dq_2
