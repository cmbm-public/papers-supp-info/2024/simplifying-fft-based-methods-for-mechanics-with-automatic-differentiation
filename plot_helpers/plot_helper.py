import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.font_manager as font_manager
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from matplotlib import ticker
from matplotlib import gridspec
from matplotlib.collections import LineCollection
from matplotlib.ticker import FormatStrFormatter

from labellines import labelLine, labelLines

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.weight'] = 'light'
mpl.rcParams["font.serif"] = ["Times New Roman"] + mpl.rcParams["font.serif"]
mpl.rcParams['font.stretch'] = 'condensed'

mpl.rcParams['font.size'] = 8
mpl.rcParams['legend.fontsize'] = 8
mpl.rcParams['savefig.format'] = 'pdf'
mpl.rcParams['mathtext.fontset'] = 'cm'

mpl.rcParams['xtick.major.width'] = 0.5
mpl.rcParams['xtick.minor.width'] = 0.5

mpl.rcParams['ytick.major.width'] = 0.5
mpl.rcParams['ytick.minor.width'] = 0.5

mpl.rcParams['axes.linewidth'] = 0.5 # set the value globally


mpl.rcParams['figure.dpi'] = 150
mpl.rcParams['savefig.dpi'] = 200
mpl.rcParams['savefig.transparent'] = True



def set_size(fraction=0.5, height='golden', width=360.24411):
    fig_width_mm = width * fraction
    inches_per_mm = 1.0 / 25.4
    if height == 'golden':
        ratio = (np.sqrt(5) - 1.0) / 2.0
        fig_height_mm = fig_width_mm * ratio
    else:
        fig_height_mm = height
    fig_width_in = fig_width_mm * inches_per_mm
    fig_height_in = fig_height_mm * inches_per_mm
    fig_dim = (fig_width_in, fig_height_in)

    return fig_dim

mpl.rcParams['figure.figsize'] = set_size(fraction=0.5)
mpl.rcParams['xtick.direction'] = 'in'
mpl.rcParams['ytick.direction'] = 'in'


class OOMFormatter(mpl.ticker.ScalarFormatter):
    def __init__(self, order=0, fformat="%1.1f", offset=True, mathText=True):
        self.oom = order
        self.fformat = fformat
        mpl.ticker.ScalarFormatter.__init__(self,useOffset=offset,useMathText=mathText)
    def _set_order_of_magnitude(self):
        self.orderOfMagnitude = self.oom
    def _set_format(self, vmin=None, vmax=None):
        self.format = self.fformat
        if self._useMathText:
             self.format = r'$\mathdefault{%s}$' % self.format
                
import numpy as np
from mpl3d import glm
from mpl3d.mesh import Mesh
from mpl3d.camera import Camera

cube = {
    "vertices":  [ [+1,+1,+1],  # A
                   [-1,+1,+1],  # B
                   [-1,-1,+1],  # C
                   [+1,-1,+1],  # D
                   [+1,-1,-1],  # E
                   [+1,+1,-1],  # F
                   [-1,+1,-1],  # G
                   [-1,-1,-1] ], # H
    "faces" :  [ [0, 1, 2, 3],   # ABCD: top face
                 [0, 3, 4, 5],   # ADEF: right face
                 [0, 5, 6, 1],   # AFGB: front face
                 [1, 6, 7, 2],   # BGHC: left face
                 [7, 4, 3, 2],   # HEDC: back face
                 [4, 7, 6, 5] ]  # EFGH: bottom face
}

class Scatter:
    def __init__(self, ax, transform, vertices, sizes, facecolors):
        self.vertices = vertices
        self.facecolors = facecolors
        self.sizes = sizes
        self.scatter = ax.scatter([], [], clip_on=False)
        self.update(transform)
        
    def update(self, transform):
        vertices = glm.transform(self.vertices, transform)
        I = np.argsort(-vertices[:,2])
        vertices = vertices[I]
        facecolors = self.facecolors[I]
        sizes = self.sizes[I]
        self.scatter.set_offsets(vertices[:,:2])
        self.scatter.set_facecolors(facecolors)
        self.scatter.set_sizes(sizes)
        self.scatter.set_antialiaseds(False)
        self.scatter.set_edgecolors("None")
